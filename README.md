# ReactJS Best practices
> Best practices for using ReactJS at NMS
> 
> Also consider reading
> * [Javascript best practices](https://code.europa.eu/digit-c4/dev/js-best-practices)
> * [generic best practices](https://code.europa.eu/digit-c4/dev/best-practices)

## Boilerplate
### React component library
Whenever it is needed to publish a React Component as library, a boilerplate is available
```shell
PROJECT_NAME=project-name
# Clone this repository locally, according to best practices
mkdir -p ~/Workspace/code.europa.eu/digit-c4/dev
git clone git@code.europa.eu:digit-c4/dev/react-best-practices.git ~/Workspace/code.europa.eu/digit-c4/dev/react-best-practices
# copy the 'component-library-boilerplate' directory as your new project root
cp ~/Workspace/code.europa.eu/digit-c4/dev/react-best-practices/component-library-boilerplate $PROJECT_NAME
cd $PROJECT_NAME && git init
```

enjoy
