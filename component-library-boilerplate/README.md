# application-landscape-viewer

## Contributing
```shell
npm install
# Start and open Storybook
npm run dev
# Or start demo application
npm run demo
```

## Publish
The demonstration
```
npm run demo-build
```
or the library
```
npm run build
```
